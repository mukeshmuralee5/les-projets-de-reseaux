# TP4 : Vers un réseau d'entreprise

### 3. Setup topologie 1

- On change l'ip du pc1 & pc2

```
PC1> ip 10.1.1.1/24
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0
```
```
PC2> ip 10.1.1.2/24
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0
```

- On ping pc2 depuis pc1
```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=2.394 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=2.045 ms
[...]
```


### 3. Setup topologie 2

- On change l'ip du pc3
```
PC3> ip 10.1.1.3/24
Checking for duplicate address...
PC3 : 10.1.1.3 255.255.255.0
```

Toutes les machines se ping :

- pc1 vers pc2 et pc3
```
PC1> ping 10.1.1.2 

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=2.601 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=2.621 ms


PC1> ping 10.1.1.3

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=3.130 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=1.128 ms
```

- pc2 vers pc1 et pc3
```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=3.565 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=3.120 ms


PC2> ping 10.1.1.3

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=1.798 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=1.116 ms

```

- pc3 vers pc1 et pc2
```
PC3> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=3.666 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=3.435 ms


PC3> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=3.461 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=4.445 ms

```

### Configuration des VLANs

Sur le switch : 
- On créé les 2 vlans

```
Switch>enable
Switch#conf t
Switch(config)#vlan 10
Switch(config-vlan)#name admins
```
```
Switch(config)#vlan 20
Switch(config-vlan)#name guests
```

- On affiche les vlans

```
Switch#show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
[...]
10   admins                           active
20   guests                           active
[...]

VLAN Type  SAID       MTU   Parent RingNo BridgeNo Stp  BrdgMode Trans1 Trans2
---- ----- ---------- ----- ------ ------ -------- ---- -------- ------ ------
[...]
10   enet  100010     1500  -      -      -        -    -        0      0
20   enet  100020     1500  -      -      -        -    -        0      0
[...]

```

- ajout des ports du switches dans le bon VLAN
- 
```
- pc1
Switch(config)#interface gigabitEthernet0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10

- pc2
Switch(config)#interface gigabitEthernet0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10

- pc3
Switch(config)#interface gigabitEthernet0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 20

```
```
Switch#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
[...]
10   admins                           active    Gi0/0, Gi0/1
20   guests                           active    Gi0/2
[...]
```

### PING... 

- pc1 et pc2 ce ping 
```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=3.021 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=2.334 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=2.445 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=2.193 ms
84 bytes from 10.1.1.2 icmp_seq=5 ttl=64 time=2.054 ms
```
```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=7.443 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=4.153 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=4.021 ms
84 bytes from 10.1.1.1 icmp_seq=4 ttl=64 time=3.242 ms
84 bytes from 10.1.1.1 icmp_seq=5 ttl=64 time=11.511 ms
```

Mais ne peuvent pas ping pc3 et inversement

```
PC2> ping 10.1.1.3

host (10.1.1.3) not reachable
```
```
PC1> ping 10.1.1.3

host (10.1.1.3) not reachable
```

```
PC3> ping 10.1.1.1

host (10.1.1.1) not reachable

PC3> ping 10.1.1.2

host (10.1.1.2) not reachable
```

## III. Routing

- Adressage ip 

```
pc1> show ip

NAME        : pc1[1]
IP/MASK     : 10.1.1.1/24
[...]
```
```
pc2> show ip

NAME        : pc2[1]
IP/MASK     : 10.1.1.2/24
[...]
```
```
adm1> show ip

NAME        : PC3[1]
IP/MASK     : 10.2.2.1/24
```

```
[admin@web1 ~]$ ip a
[...]
2: enp0s3:
    inet 10.3.3.1/24 brd 10.3.3.255 scope global noprefixroute enp0s3
[...]

```



- On créé les 3 vlans 
```
Switch#conf t

Switch(config)#vlan 11
Switch(config-vlan)#name clients

Switch(config)#vlan 12
Switch(config-vlan)#name admins

Switch(config)#vlan 13
Switch(config-vlan)#name servers
```

- On donne attribué les vlans aux interfaces

```
Switch(config)#interface gigabitEthernet0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11


Switch(config)#interface gigabitEthernet0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11


Switch(config)#interface gigabitEthernet0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 12


Switch(config)#interface gigabitEthernet0/3
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 13

```

Ajout du routeur comme un trunk
```
Switch#conf t
Switch(config)#interface gigabitEthernet1/1
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11,12,13

Switch#show interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi1/1       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi1/1       1-4094

Port        Vlans allowed and active in management domain
Gi1/1       1,10-13,20

Port        Vlans in spanning tree forwarding state and not pruned
Gi1/1       none
```

- Config du routeur

```
R1(config)#interface fastEthernet 0/0.11
R1(config-subif)#encapsulation dot1Q 11
R1(config-subif)#ip addr 10.1.1.254 255.255.255.0


R1(config)#interface fastEthernet 0/0.12
R1(config-subif)#encapsulation dot1Q 12
R1(config-subif)#ip addr 10.2.2.254 255.255.255.0


R1(config)#interface fastEthernet 0/0.13
R1(config-subif)#encapsulation dot1Q 13
R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
```
- On up notre interface
```
R1(config)#interface fastEthernet 0/0
R1(config-subif)# no shutdown
```

- ping vers le router

- pc1
```
pc1> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=9.669 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=6.716 ms
[...]
```

- pc2
```
pc2> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=9.624 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=11.108 ms
[...]
```

- adm1
```
adm1> ping 10.2.2.254

84 bytes from 10.2.2.254 icmp_seq=1 ttl=255 time=9.883 ms
84 bytes from 10.2.2.254 icmp_seq=2 ttl=255 time=5.362 ms
[...]

```

- web1
```
[admin@web1 ~]$ ping 10.3.3.254
PING 10.3.3.254 (10.3.3.254) 56(84) bytes of data.
64 bytes from 10.3.3.254: icmp_seq=1 ttl=255 time=10.9 ms
64 bytes from 10.3.3.254: icmp_seq=2 ttl=255 time=23.4 ms
[...]
```

### On donne à nos pc l'adresse de la gateway
- Pour les pcs : 

```
pc1> ip 10.1.1.1 10.1.1.254
```
```
pc2> ip 10.1.1.2 10.1.1.254
```
```
adm1> ip 10.2.2.1 10.2.2.254
```

- Pour la vm web1 on ajoute la ligne suivant dans le fichier ifcfg-enp0s3 contenue dans /etc/sysconfig/network-scripts/
```
GATEWAY=10.3.3.254
```

- Ping de pc1 vers adm1 et web1

```
pc1> ping 10.2.2.1

84 bytes from 10.2.2.1 icmp_seq=1 ttl=63 time=33.550 ms
84 bytes from 10.2.2.1 icmp_seq=2 ttl=63 time=28.451 ms


pc1> ping 10.3.3.1

84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=15.804 ms
84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=38.330 ms

```
- Ping de web1 vers pc2 et adm1
```
[admin@web1 ~]$ ping 10.1.1.2
PING 10.1.1.2 (10.1.1.2) 56(84) bytes of data.
64 bytes from 10.1.1.2: icmp_seq=2 ttl=63 time=26.3 ms
64 bytes from 10.1.1.2: icmp_seq=3 ttl=63 time=36.0 ms
[...]

[admin@web1 ~]$ ping 10.2.2.1
PING 10.2.2.1 (10.2.2.1) 56(84) bytes of data.
64 bytes from 10.2.2.1: icmp_seq=1 ttl=63 time=17.6 ms
64 bytes from 10.2.2.1: icmp_seq=2 ttl=63 time=21.8 ms
[...]
```

## IV. NAT

### 3. Setup topologie 4

- On récupére une ip en dhcp
```
R1#conf t
R1(config)#interface fastEthernet 1/0
R1(config-if)#ip address dhcp
R1(config-if)#no shut
```

### On peut ping 1.1.1.1
- Depuis web1
```
[admin@web1 ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=3 ttl=62 time=24.10 ms
64 bytes from 1.1.1.1: icmp_seq=4 ttl=62 time=31.10 ms
[...]
```
- Depuis pc1
```
pc1> ping 1.1.1.1
84 bytes from 1.1.1.1 icmp_seq=1 ttl=62 time=25.611 ms
84 bytes from 1.1.1.1 icmp_seq=2 ttl=62 time=28.575 ms
```

- Configurez le NAT
```
R1(config)#interface fastEthernet 0/0
R1(config-if)#ip nat inside


R1(config)#interface fastEthernet 1/0
R1(config-if)#ip nat outside


R1(config)#access-list 1 permit any
R1(config)#ip nat inside source list 1 interface fastEthernet 1/0 overload
```

### Test

```
ip dns <ip du dns>

ip dns 1.1.1.1
``` 

Sur la machine Linux on ajoute la ligne suivant dans le fichier ifcfg-enp0s3 contenue dans /etc/sysconfig/network-scripts/
```
DNS1=1.1.1.1
```

Ping depuis pc1 
```
pc1> ping google.com
google.com resolved to 172.217.18.206

84 bytes from 172.217.18.206 icmp_seq=1 ttl=112 time=40.898 ms
84 bytes from 172.217.18.206 icmp_seq=2 ttl=112 time=36.592 ms

```

Ping depuis la machine linux 
```
[admin@web1 ~]$ ping google.com
PING google.com (142.250.179.110) 56(84) bytes of data.
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=1 ttl=115 time=39.5 ms
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=2 ttl=115 time=38.1 ms

```
---

- Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `admins`  | `10.2.2.0/24` | 12           |
| `servers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node                | `clients`       | `admins`        | `servers`       |
|---------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`   | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`   | `10.1.1.2/24`   | x               | x               |
| `pc3.clients.tp4`   | DHCP            | x               | x               |
| `pc4.clients.tp4`   | DHCP            | x               | x               |
| `pc5.clients.tp4`   | DHCP            | x               | x               |
| `dhcp1.clients.tp4` | `10.1.1.253/24` | x               | x               |
| `adm1.admins.tp4`   | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4`  | x               | x               | `10.3.3.1/24`   |
| `r1`                | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

