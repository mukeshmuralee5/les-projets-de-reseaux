# TP1 - Mise en jambe

## I. Exploration locale en solo



- nom, adresse MAC et adresse IP de l'interface WiFi
- nom, adresse MAC et adresse IP de l'interface Ethernet

````  
 Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .:
   fe80::55f7:b798:6eb9:f8ac%18
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.2.46
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253  
   
  
  Carte Ethernet Ethernet :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Realtek PCIe GbE Family Controller
   Adresse physique . . . . . . . . . . . : 3C-7C-3F-1A-6C-49
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   
   ```` 

   


 - utilisez une commande pour connaître l'adresse IP de la passerelle de votre carte WiFi 


     ```` Passerelle par défaut. . . . . . . . . : 10.33.3.253 ```` 


 
- En graphique (GUI : Graphical User Interface)
    En utilisant l'interface graphique de votre OS :
  
  Trouvez l'IP, la MAC et la gateway pour l'interface WiFi de votre PC
  
  ```` Pour trouver ces éléments, il faut se rendre dans le panneau de configuration puis Reseau et Internet ensuite Connexions reseau enfin on y trouve le reseau WiFi en accédant aux détails nous avons les informations dont nous avons besoin. ````
  
    ![](https://i.imgur.com/mJC2qXL.png)
    
    
- à quoi sert la gateway dans le réseau d'YNOV ?
    
  ```
  
  ```
     ----
     
     
## 2. Modifications des informations

-  Modification d'adresse IP (part 1)
    Utilisez l'interface graphique de votre OS pour changer d'adresse IP :
    
    Pour faire la manipulation suivante, il faut se rendre dans Panneau de configuration, 
    
    ![](https://i.imgur.com/Weejz8x.png)


----

## B. Table ARP

- Exploration de la table ARP

    depuis la ligne de commande, afficher la table ARP
    identifier l'adresse MAC de la passerelle de votre           réseau, et expliquer comment vous avez repéré cette adresse MAC spécifiquement.
    
  -> la ligne de commande qui permet d'acceder au Table ARP : 
     ```
     PS C:\Users\Muralee> arp -a
     ```
     
     A ce stade là, On connait l'adresse IP de la carte Wifi ( 10.33.2.46 ) de cet ordinateur. Dans la table ARP, nous reconnaissons la carte de Wifi. Puis, pour trouver l'adresse MAC, il suffit de prendre l'adresse MAC suivante ( 10.33.2.48 ) qui suit l'adresse IP ( 10.33.2.46 ) de cette carte Wifi.
     


    ``` 
    Interface : 10.33.2.46 --- 0x12
    
      Adresse Internet      Adresse physique      Type
    
      10.33.2.48            80-91-33-9c-bf-0d     dynamique

    ```
    
    
    Exploration de la table ARP

depuis la ligne de commande, afficher la table ARP
identifier l'adresse MAC de la passerelle de votre réseau, et expliquer comment vous avez repéré cette adresse MAC spécifiquement

🌞 Et si on remplissait un peu la table ?

- envoyez des ping vers des IP du même réseau que vous. Lesquelles ? menfou, random. Envoyez des ping vers au moins 3-4 machines.

     ```  
       1ère Machine : 
       
            PS C:\Users\Muralee> ping 10.33.0.250

    Envoi d’une requête 'Ping'  10.33.0.250 avec 32 octets de données :
    Réponse de 10.33.0.250 : octets=32 temps=4 ms TTL=128
    Réponse de 10.33.0.250 : octets=32 temps=9 ms TTL=128
    Réponse de 10.33.0.250 : octets=32 temps=7 ms TTL=128
    Réponse de 10.33.0.250 : octets=32 temps=18 ms TTL=128

    Statistiques Ping pour 10.33.0.250:
        Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
    Durée approximative des boucles en millisecondes :
        Minimum = 4ms, Maximum = 18ms, Moyenne = 9ms 


        2ème machine : 

        PS C:\Users\Muralee> ping   10.33.10.155

    Envoi d’une requête 'Ping'  10.33.10.155 avec 32 octets de données :
    Réponse de 10.33.10.155 : octets=32 temps=2 ms TTL=63
    Réponse de 10.33.10.155 : octets=32 temps=4 ms TTL=63
    Réponse de 10.33.10.155 : octets=32 temps=6 ms TTL=63
    Réponse de 10.33.10.155 : octets=32 temps=3 ms TTL=63

    Statistiques Ping pour 10.33.10.155:
        Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
    Durée approximative des boucles en millisecondes :
        Minimum = 2ms, Maximum = 6ms, Moyenne = 3ms


        3ème machine :

        PS C:\Users\Muralee> ping 10.33.0.135

    Envoi d’une requête 'Ping'  10.33.0.135 avec 32 octets de données :
    Réponse de 10.33.0.135 : octets=32 temps=9 ms TTL=128
    Réponse de 10.33.0.135 : octets=32 temps=5 ms TTL=128
    Réponse de 10.33.0.135 : octets=32 temps=39 ms TTL=128
    Réponse de 10.33.0.135 : octets=32 temps=6 ms TTL=128

    Statistiques Ping pour 10.33.0.135:
        Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
    Durée approximative des boucles en millisecondes :
        Minimum = 5ms, Maximum = 39ms, Moyenne = 14ms



    ```



- affichez votre table ARP

``` 
Interface : 10.33.2.46 --- 0x12
  Adresse Internet      Adresse physique      Type
  3.209.100.255         00-12-00-40-4c-bf     dynamique
  3.212.220.98          00-12-00-40-4c-bf     dynamique
  8.241.17.254          00-12-00-40-4c-bf     dynamique
  8.247.205.254         00-12-00-40-4c-bf     dynamique
  10.0.1.1              00-12-00-40-4c-bf     dynamique
  10.0.1.2              00-12-00-40-4c-bf     dynamique
  10.0.255.240          00-12-00-40-4c-bf     dynamique
  10.33.0.6             84-5c-f3-80-32-07     dynamique
  10.33.0.99            e0-cc-f8-99-2b-27     dynamique
  10.33.0.135           f8-5e-a0-06-40-d2     dynamique
  10.33.0.140           40-ec-99-8b-11-c2     dynamique
  10.33.0.228           7c-5c-f8-2d-40-42     dynamique
  10.33.0.250           28-cd-c4-dd-db-73     dynamique
  10.33.1.79            90-cc-df-09-80-a8     dynamique
  10.33.1.123           48-e7-da-29-9f-e1     dynamique
  10.33.1.142           74-d8-3e-0d-06-b0     dynamique
  10.33.1.228           34-7d-f6-b2-82-cf     dynamique
  10.33.1.248           e8-84-a5-24-94-c9     dynamique
  10.33.2.4             f0-9e-4a-64-4b-2d     dynamique
  10.33.2.48            80-91-33-9c-bf-0d     dynamique
  10.33.2.88            68-3e-26-7c-c3-1a     dynamique
  10.33.2.182           f4-4e-e3-c0-ed-29     dynamique
  10.33.2.196           08-71-90-87-b9-8c     dynamique
  10.33.2.201           d8-3b-bf-98-b1-6f     dynamique
  10.33.2.211           08-d2-3e-55-63-1a     dynamique
  10.33.3.50            38-87-d5-d7-71-2a     dynamique
  10.33.3.88            4c-02-20-4b-a9-d9     dynamique
  10.33.3.99            b8-9a-2a-dc-0b-6d     dynamique
  10.33.3.119           58-6c-25-82-17-c5     dynamique
  10.33.3.123           38-fc-98-e5-ae-61     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  10.33.8.244           00-12-00-40-4c-bf     dynamique
  10.33.10.2            00-12-00-40-4c-bf     dynamique
  10.33.10.148          00-12-00-40-4c-bf     dynamique
  10.33.10.155          00-12-00-40-4c-bf     dynamique
  13.89.178.26          00-12-00-40-4c-bf     dynamique
  13.89.178.27          00-12-00-40-4c-bf     dynamique
  13.89.179.10          00-12-00-40-4c-bf     dynamique
  13.107.4.52           00-12-00-40-4c-bf     dynamique
  13.107.43.12          00-12-00-40-4c-bf     dynamique
  20.42.65.88           00-12-00-40-4c-bf     dynamique
  20.42.65.90           00-12-00-40-4c-bf     dynamique
  20.54.37.73           00-12-00-40-4c-bf     dynamique
  20.54.110.119         00-12-00-40-4c-bf     dynamique
  20.67.183.221         00-12-00-40-4c-bf     dynamique
  20.101.57.9           00-12-00-40-4c-bf     dynamique
  20.191.46.109         00-12-00-40-4c-bf     dynamique
  23.15.179.186         00-12-00-40-4c-bf     dynamique
  23.15.179.217         00-12-00-40-4c-bf     dynamique
  23.52.239.24          00-12-00-40-4c-bf     dynamique
  23.57.5.5             00-12-00-40-4c-bf     dynamique
  23.72.22.174          00-12-00-40-4c-bf     dynamique
  35.186.224.25         00-12-00-40-4c-bf     dynamique
  40.90.65.45           00-12-00-40-4c-bf     dynamique
  40.125.122.151        00-12-00-40-4c-bf     dynamique
  40.126.31.1           00-12-00-40-4c-bf     dynamique
  40.126.31.7           00-12-00-40-4c-bf     dynamique
  40.127.240.158        00-12-00-40-4c-bf     dynamique
  51.11.168.232         00-12-00-40-4c-bf     dynamique
  52.21.171.126         00-12-00-40-4c-bf     dynamique
  52.72.168.207         00-12-00-40-4c-bf     dynamique
  52.98.145.66          00-12-00-40-4c-bf     dynamique
  52.137.110.235        00-12-00-40-4c-bf     dynamique
  52.182.141.63         00-12-00-40-4c-bf     dynamique
  52.182.143.208        00-12-00-40-4c-bf     dynamique
  52.183.24.194         00-12-00-40-4c-bf     dynamique
  52.184.213.21         00-12-00-40-4c-bf     dynamique
  64.233.184.188        00-12-00-40-4c-bf     dynamique
  75.2.77.152           00-12-00-40-4c-bf     dynamique
  82.145.216.15         00-12-00-40-4c-bf     dynamique
  82.145.216.16         00-12-00-40-4c-bf     dynamique
  99.83.179.177         00-12-00-40-4c-bf     dynamique
  104.18.30.151         00-12-00-40-4c-bf     dynamique
  104.115.83.107        00-12-00-40-4c-bf     dynamique
  104.208.16.90         00-12-00-40-4c-bf     dynamique
  142.93.179.80         00-12-00-40-4c-bf     dynamique
  165.22.45.123         00-12-00-40-4c-bf     dynamique
  185.26.182.123        00-12-00-40-4c-bf     dynamique
  204.79.197.203        00-12-00-40-4c-bf     dynamique
  209.197.3.8           00-12-00-40-4c-bf     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
  ```




- listez les adresses MAC associées aux adresses IP que vous avez ping

```
  Adresse Internet      Adresse physique      Type
10.33.0.250           28-cd-c4-dd-db-73     dynamique
------------------------------------------------------
10.33.10.155          00-12-00-40-4c-bf     dynamique
------------------------------------------------------
10.33.0.135           f8-5e-a0-06-40-d2     dynamique

```
    
    
    

## Nmap



- Réinitialiser votre conf réseau (reprenez une IP automatique, en vous déconnectant/reconnectant au réseau par exemple)

 ![](https://i.imgur.com/pnLLCpT.png)


- lancez un scan de ping sur le réseau YNOV

```
PS C:\Users\Muralee> nmap -sn -n 10.33.0.0/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 11:27 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 10.33.0.6
Host is up (0.0030s latency).
MAC Address: 84:5C:F3:80:32:07 (Intel Corporate)
Nmap scan report for 10.33.0.7
Host is up (0.19s latency).
MAC Address: 9C:BC:F0:B6:1B:ED (Xiaomi Communications)
Nmap scan report for 10.33.0.19
Host is up (1.00s latency).
MAC Address: D4:A3:3D:C8:CA:67 (Apple)
Nmap scan report for 10.33.0.21
Host is up (0.0060s latency).
MAC Address: B0:FC:36:CE:9C:89 (CyberTAN Technology)
Nmap scan report for 10.33.0.27
Host is up (0.0060s latency).
MAC Address: A8:64:F1:8B:1D:4D (Intel Corporate)
Nmap scan report for 10.33.0.31
Host is up (0.54s latency).
MAC Address: FA:C5:6D:60:4B:4C (Unknown)
Nmap scan report for 10.33.0.42
Host is up (0.64s latency).
MAC Address: CE:A6:A5:8C:F3:7A (Unknown)
Nmap scan report for 10.33.0.43
Host is up (0.0080s latency).
MAC Address: 2C:8D:B1:94:38:BF (Intel Corporate)
Nmap scan report for 10.33.0.57
Host is up (0.053s latency).
MAC Address: F0:18:98:8C:6F:CD (Apple)
Nmap scan report for 10.33.0.59
Host is up (0.20s latency).
MAC Address: E4:0E:EE:73:73:96 (Huawei Technologies)
Nmap scan report for 10.33.0.60
Host is up (0.43s latency).
MAC Address: E2:EE:36:A5:0B:8A (Unknown)
Nmap scan report for 10.33.0.71
Host is up (0.087s latency).
MAC Address: F0:03:8C:35:FE:47 (AzureWave Technology)
Nmap scan report for 10.33.0.96
Host is up (0.017s latency).
MAC Address: CA:4F:F4:AF:8F:0C (Unknown)
Nmap scan report for 10.33.0.99
Host is up (0.032s latency).
MAC Address: E0:CC:F8:99:2B:27 (Xiaomi Communications)
Nmap scan report for 10.33.0.100
Host is up (0.0070s latency).
MAC Address: E8:6F:38:6A:B6:EF (Chongqing Fugui Electronics)
Nmap scan report for 10.33.0.135
Host is up (0.0059s latency).
MAC Address: F8:5E:A0:06:40:D2 (Intel Corporate)
Nmap scan report for 10.33.0.140
Host is up (0.0036s latency).
MAC Address: 40:EC:99:8B:11:C2 (Intel Corporate)
Nmap scan report for 10.33.0.143
Host is up (0.082s latency).
MAC Address: F0:18:98:41:11:07 (Apple)
Nmap scan report for 10.33.0.180
Host is up (0.12s latency).
MAC Address: 26:91:29:98:E2:9D (Unknown)
Nmap scan report for 10.33.0.212
Host is up (0.23s latency).
MAC Address: 48:2C:A0:C8:F1:DC (Xiaomi Communications)
Nmap scan report for 10.33.0.217
Host is up (0.24s latency).
MAC Address: 76:8F:9C:ED:EC:B1 (Unknown)
Nmap scan report for 10.33.0.221
Host is up (0.41s latency).
MAC Address: 84:C5:A6:EB:C3:A5 (Intel Corporate)

[...]

MAC Address: 8C:85:90:65:6A:52 (Apple)
Nmap scan report for 10.33.3.219
Host is up (0.21s latency).
MAC Address: A0:78:17:B5:63:BB (Apple)
Nmap scan report for 10.33.3.226
Host is up (0.042s latency).
MAC Address: 5C:3A:45:06:7A:5F (Chongqing Fugui Electronics)
Nmap scan report for 10.33.3.227
Host is up (0.0050s latency).
MAC Address: 36:EC:48:54:F0:19 (Unknown)
Nmap scan report for 10.33.3.232
Host is up (0.13s latency).
MAC Address: 88:66:5A:4D:A7:F5 (Apple)
Nmap scan report for 10.33.3.236
Host is up (0.049s latency).
MAC Address: 3E:02:9D:7D:30:8F (Unknown)
Nmap scan report for 10.33.3.239
Host is up (0.57s latency).
MAC Address: 38:F9:D3:AE:C3:A3 (Apple)
Nmap scan report for 10.33.3.249
Host is up (0.012s latency).
MAC Address: 58:96:1D:17:43:F5 (Intel Corporate)
Nmap scan report for 10.33.3.252
Host is up (0.0080s latency).
MAC Address: 00:1E:4F:F9:BE:14 (Dell)
Nmap scan report for 10.33.3.253
Host is up (0.0064s latency).
MAC Address: 00:12:00:40:4C:BF (Cisco Systems)
Nmap scan report for 10.33.3.254
Host is up (0.0040s latency).
MAC Address: 00:0E:C4:CD:74:F5 (Iskra Transmission d.d.)
Nmap scan report for 10.33.2.46
Host is up.
Nmap done: 1024 IP addresses (107 hosts up) scanned in 29.96 seconds

```





- Affichez votre table ARP

``` 

Interface : 10.33.2.46 --- 0x12
  Adresse Internet      Adresse physique      Type
  2.16.209.124          00-12-00-40-4c-bf     dynamique
  3.209.100.255         00-12-00-40-4c-bf     dynamique
  3.212.220.98          00-12-00-40-4c-bf     dynamique
  3.217.47.173          00-12-00-40-4c-bf     dynamique
  3.225.123.42          00-12-00-40-4c-bf     dynamique
  8.241.17.254          00-12-00-40-4c-bf     dynamique
  8.247.205.254         00-12-00-40-4c-bf     dynamique
  10.0.1.1              00-12-00-40-4c-bf     dynamique
  10.0.1.2              00-12-00-40-4c-bf     dynamique
  10.0.255.240          00-12-00-40-4c-bf     dynamique
  10.33.0.6             84-5c-f3-80-32-07     dynamique
  10.33.0.7             9c-bc-f0-b6-1b-ed     dynamique
  10.33.0.31            fa-c5-6d-60-4b-4c     dynamique
  10.33.0.34            ac-67-5d-83-9f-e6     dynamique
  10.33.0.42            ce-a6-a5-8c-f3-7a     dynamique
  10.33.0.43            2c-8d-b1-94-38-bf     dynamique
  10.33.0.48            54-4e-90-3c-33-3f     dynamique
  10.33.0.60            e2-ee-36-a5-0b-8a     dynamique
  10.33.0.78            ec-63-d7-c7-51-87     dynamique
  10.33.0.96            ca-4f-f4-af-8f-0c     dynamique
  10.33.0.99            e0-cc-f8-99-2b-27     dynamique
  10.33.0.111           d2-41-f0-dc-6a-ed     dynamique
  10.33.0.119           18-56-80-70-9c-48     dynamique
  10.33.0.135           f8-5e-a0-06-40-d2     dynamique
  10.33.0.140           40-ec-99-8b-11-c2     dynamique
  
                      [...]
          

  20.54.110.119         00-12-00-40-4c-bf     dynamique
  20.67.183.221         00-12-00-40-4c-bf     dynamique
  20.82.209.183         00-12-00-40-4c-bf     dynamique
  20.101.57.9           00-12-00-40-4c-bf     dynamique
  20.189.173.2          00-12-00-40-4c-bf     dynamique
  20.190.160.9          00-12-00-40-4c-bf     dynamique
  20.191.46.109         00-12-00-40-4c-bf     dynamique
  23.15.179.186         00-12-00-40-4c-bf     dynamique
  23.15.179.217         00-12-00-40-4c-bf     dynamique
  23.52.239.24          00-12-00-40-4c-bf     dynamique
  23.57.5.5             00-12-00-40-4c-bf     dynamique
  23.57.81.51           00-12-00-40-4c-bf     dynamique
  23.72.17.56           00-12-00-40-4c-bf     dynamique
  23.72.22.174          00-12-00-40-4c-bf     dynamique
  23.97.153.169         00-12-00-40-4c-bf     dynamique
  34.231.82.104         00-12-00-40-4c-bf     dynamique
  35.186.224.25         00-12-00-40-4c-bf     dynamique
  37.187.24.36          00-12-00-40-4c-bf     dynamique
  40.74.108.79          00-12-00-40-4c-bf     dynamique
  40.90.65.45           00-12-00-40-4c-bf     dynamique
  40.112.88.60          00-12-00-40-4c-bf     dynamique
  40.125.122.151        00-12-00-40-4c-bf     dynamique
  40.126.31.1           00-12-00-40-4c-bf     dynamique
  40.126.31.7           00-12-00-40-4c-bf     dynamique
  40.127.240.158        00-12-00-40-4c-bf     dynamique
  51.11.168.232         00-12-00-40-4c-bf     dynamique
  51.105.71.136         00-12-00-40-4c-bf     dynamique
  51.144.113.175        00-12-00-40-4c-bf     dynamique
  52.21.171.126         00-12-00-40-4c-bf     dynamique
  52.72.168.207         00-12-00-40-4c-bf     dynamique
  52.73.189.143         00-12-00-40-4c-bf     dynamique
  52.84.174.4           00-12-00-40-4c-bf     dynamique
  52.86.215.46          00-12-00-40-4c-bf     dynamique
  52.98.145.66          00-12-00-40-4c-bf     dynamique
  52.109.76.32          00-12-00-40-4c-bf     dynamique
  52.137.110.235        00-12-00-40-4c-bf     dynamique
  52.142.114.176        00-12-00-40-4c-bf     dynamique
  52.178.17.3           00-12-00-40-4c-bf     dynamique
  52.182.141.63         00-12-00-40-4c-bf     dynamique
  52.182.143.208        00-12-00-40-4c-bf     dynamique
  52.183.24.194         00-12-00-40-4c-bf     dynamique
  52.184.213.21         00-12-00-40-4c-bf     dynamique
  52.218.109.0          00-12-00-40-4c-bf     dynamique
  52.218.109.216        00-12-00-40-4c-bf     dynamique
  54.156.15.99          00-12-00-40-4c-bf     dynamique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
  
  ```

 Modifiez de nouveau votre adresse IP vers une adresse IP que vous savez libre grâce à nmap :
 
 Jai trouvé une adresse IP libre grâce a la commande suivante : 
```
     PS C:\Users\Muralee> nmap -n -sP 10.33.2.46
``` 


 ``` 
 Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Realtek 8822CE Wireless LAN 802.11ac PCI-E NIC
   Adresse physique . . . . . . . . . . . : 80-30-49-CB-43-8F
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::55f7:b798:6eb9:f8ac%18(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.3.242(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
 
   
   ```


configurez correctement votre gateway pour avoir accès à d'autres réseaux (utilisez toujours la gateway d'YNOV)
prouvez en une suite de commande que vous avez une IP choisi manuellement, que votre passerelle est bien définie, et que vous avez un accès internet

```

PS C:\Users\Muralee> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=45 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=95 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=34 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=88 ms TTL=115

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 34ms, Maximum = 95ms, Moyenne = 65ms
```

----

### II. Exploration locale en duo

Owkay. Vous savez à ce stade :

afficher les informations IP de votre machine
modifier les informations IP de votre machine
c'est un premier pas vers la maîtrise de votre outil de travail

On va maintenant répéter un peu ces opérations, mais en créant un réseau local de toutes pièces : entre deux PCs connectés avec un câble RJ45.

- 1. Prérequis

deux PCs avec ports RJ45
un câble RJ45

firewalls désactivés sur les deux PCs


- 2. Câblage
Ok c'est la partie tendue. Prenez un câble. Branchez-le des deux côtés. Bap.


 ## Modification d'adresse IP
 Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :

- modifiez l'IP des deux machines pour qu'elles soient dans le même réseau

choisissez une IP qui commence par "192.168"
utilisez un /30 (que deux IP disponibles)

![](https://i.imgur.com/xKU4uO8.png)


- vérifiez à l'aide de commandes que vos changements ont pris effet

Nous voyons bien ici que notre adresse IP est comme nous voulons donc 192.168.10.2
 
 ```
 PS C:\Users\Muralee> ipconfig /all

 
Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Realtek PCIe GbE Family Controller
   Adresse physique . . . . . . . . . . . : 3C-7C-3F-1A-6C-49
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::75ca:b818:94f4:af90%25(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.10.2(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
  
  
 ```

- utilisez ping pour tester la connectivité entre les deux machines
 
 
 La connexion est établi entre les deux ordinateur.
 ``` 
 PS C:\Users\Muralee> ping 192.168.10.1

Envoi d’une requête 'Ping'  192.168.10.1 avec 32 octets de données :
Réponse de 192.168.10.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.10.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.10.1 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.10.1:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 2ms, Moyenne = 2ms
    
```
 
- Affichez et consultez votre table ARP

Nous trouvons ici la commande qui permets d'acceder à notre table ARP puis les informations concernants notre carte reseau Ethernet.

```
PS C:\Users\Muralee> arp -a

Interface : 192.168.10.2 --- 0x19
  Adresse Internet      Adresse physique      Type
  192.168.10.1          b4-a9-fc-9f-9b-46     dynamique
  192.168.10.3          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

```


## 4. Utilisation d'un des deux comme gateway
 
- pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu 

```
PS C:\Users\Muralee> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=22 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=20 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=114

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 20ms, Maximum = 22ms, Moyenne = 21ms
 

```



- utiliser un traceroute ou tracert pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)

```
PS C:\Users\Muralee> tracert 192.168.10.1

Détermination de l’itinéraire vers LAPTOP-FLEOPDHT [192.168.10.1]
avec un maximum de 30 sauts :

  1     1 ms     1 ms     1 ms  LAPTOP-FLEOPDHT [192.168.10.1]

Itinéraire déterminé.
PS C:\Users\Muralee> tracert 1.1.1.1

Détermination de l’itinéraire vers 1.1.1.1 avec un maximum de 30 sauts.

  1     1 ms     *        2 ms  LAPTOP-FLEOPDHT [192.168.10.1]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     8 ms    12 ms     5 ms  10.33.3.253
  4     8 ms     5 ms     4 ms  10.33.10.254
  5    14 ms    10 ms    10 ms  92.103.174.137
  6    12 ms    11 ms    11 ms  92.103.120.182
  7    24 ms    22 ms    21 ms  172.19.130.117
  8    25 ms    24 ms    24 ms  46.218.128.74
  9    48 ms    22 ms    22 ms  195.42.144.143
 10    22 ms    19 ms    19 ms  1.1.1.1

Itinéraire déterminé.

```

## 5. Petit chat privé

On va créer un chat extrêmement simpliste à l'aide de netcat (abrégé nc). Il est souvent considéré comme un bon couteau-suisse quand il s'agit de faire des choses avec le réseau.


L'idée ici est la suivante :

l'un de vous jouera le rôle d'un serveur

l'autre sera le client qui se connecte au serveur




sur le PC serveur avec par exemple l'IP 192.168.1.1

nc.exe -l -p 8888

"netcat, écoute sur le port numéro 8888 stp"

il se passe rien ? Normal, faut attendre qu'un client se connecte



🌞 sur le PC client avec par exemple l'IP 192.168.1.2


nc.exe 192.168.1.1 8888

"netcat, connecte toi au port 8888 de la machine 192.168.1.1 stp"


une fois fait, vous pouvez taper des messages dans les deux sens

![](https://i.imgur.com/4gUtS0Y.png)



## 6. Firewall


- Activez votre firewall
    
    ![](https://i.imgur.com/CtUGFhB.png)



#### Autoriser les ping

- Configurer le firewall de votre OS pour accepter le ping

Pour ce faire, il faut d'abord lancer son powershell ou cmd en tant qu'administrateur puis effectuer la commande 
suivante ( nous avons aussi d'autres manieres d'effectuer ceci mais j'ai trouvé que c'était plus simple de passer par notre cmd)

```
PS C:\WINDOWS\system32> netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow
Ok.

```



🌞 Autoriser le traffic sur le port qu'utilise nc

- Ouverture de port TCP et/ou UDP



choisissez arbitrairement un port entre 1024 et 20000
vous utiliserez ce port pour communiquer avec netcat par groupe de 2 toujours
le firewall du PC serveur devra avoir un firewall activé et un netcat qui fonctionne



![](https://i.imgur.com/MrspMPQ.png)


```
Apres avoir activer et ouvert un port TCP, nous avons réalisé de nouveau le chat avec netcat.

C:\Users\Muralee\Desktop\netcat-1.11>nc.exe 192.168.10.1 8888

```


Voici le resultat qu'on obtient apres nos modifications.
(toujours la même chose :) )

![](https://i.imgur.com/Y8muXAc.png)



## III. Manipulations d'autres outils/protocoles côté client

### 1. DHCP
Bon ok vous savez définir des IPs à la main. Mais pour être dans le réseau YNOV, vous l'avez jamais fait.
C'est le serveur DHCP d'YNOV qui vous a donné une IP.
Une fois que le serveur DHCP vous a donné une IP, vous enregistrer un fichier appelé bail DHCP qui contient, entre autres :

l'IP qu'on vous a donné
le réseau dans lequel cette IP est valable

🌞Exploration du DHCP, depuis votre PC

afficher l'adresse IP du serveur DHCP du réseau WiFi YNOV
cette adresse a une durée de vie limitée. C'est le principe du bail DHCP (ou DHCP lease). Trouver la date d'expiration de votre bail DHCP
vous pouvez vous renseigner un peu sur le fonctionnement de DHCP dans les grandes lignes. On aura sûrement un cours là dessus :)

``` 

 

### 2. DNS
Le protocole DNS permet la résolution de noms de domaine vers des adresses IP. Ce protocole permet d'aller sur google.com plutôt que de devoir connaître et utiliser l'adresse IP du serveur de Google.
Un serveur DNS est un serveur à qui l'on peut poser des questions (= effectuer des requêtes) sur un nom de domaine comme google.com, afin d'obtenir les adresses IP liées au nom de domaine.
Si votre navigateur fonctionne "normalement" (il vous permet d'aller sur google.com par exemple) alors votre ordinateur connaît forcément l'adresse d'un serveur DNS. Et quand vous naviguez sur internet, il effectue toutes les requêtes DNS à votre place, de façon automatique.


🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur


🌞 utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

faites un lookup (lookup = "dis moi à quelle IP se trouve tel nom de domaine")

pour google.com

pour ynov.com

interpréter les résultats de ces commandes


déterminer l'adresse IP du serveur à qui vous venez d'effectuer ces requêtes
faites un reverse lookup (= "dis moi si tu connais un nom de domaine pour telle IP")

pour l'adresse 78.74.21.21

pour l'adresse 92.146.54.88

interpréter les résultats
si vous vous demandez, j'ai pris des adresses random :)

```
2. DNS

```
C:\Linux\netcat>ipconfig /all

Carte réseau sans fil Wi-Fi :


   Serveurs DNS. . .  . . . . . . . . . . : 192.168.1.1


--------------------------------------------------------

C:\Linux\netcat>nslookup google.com
Serveur :   Bbox.nomad
Address:  192.168.1.1

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:812::200e
          216.58.213.78


--------------------------------------------------------

C:\Linux\netcat>nslookup ynov.com
Serveur :   Bbox.nomad
Address:  192.168.1.1

Réponse ne faisant pas autorité :
Nom :    ynov.com
Addresses:  64:ff9b::5cf3:108f
          92.243.16.143


--------------------------------------------------------



C:\Linux\netcat>nslookup google.com
Serveur :   Bbox.nomad
Address:  192.168.1.1
l'adresse ip a qui on fait la requete est celle du serveur dns
C:\Cours\Cours_B2\Linux\netcat>nslookup 78.74.21.21
Serveur :   Bbox.nomad
Address:  192.168.1.1

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21


--------------------------------------------------------

C:\Linux\netcat>nslookup 92.146.54.88
Serveur :   Bbox.nomad
Address:  192.168.1.1

Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
Address:  92.146.54.88

```






## IV. Wireshark

Wireshark est un outil qui permet de visualiser toutes les trames qui sortent et entrent d'une carte réseau.
Il peut :

enregistrer le trafic réseau, pour l'analyser plus tard
afficher le trafic réseau en temps réel

On peut TOUT voir.
Un peu austère aux premiers abords, une manipulation très basique permet d'avoir une très bonne compréhension de ce qu'il se passe réellement.

téléchargez l'outil Wireshark


🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :

un ping entre vous et la passerelle
un netcat entre vous et votre mate, branché en RJ45
une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question.
prenez moi des screens des trames en question
on va prendre l'habitude d'utiliser Wireshark souvent dans les cours, pour visualiser ce qu'il se passe



###### Ping 

![](https://i.imgur.com/PTcBATU.png)



###### Netcat 

![](https://i.imgur.com/wQFQsf3.png)




###### DNS 

![](https://i.imgur.com/PaysIgM.png)




Vu pendant le TP :

visualisation de vos interfaces réseau (en GUI et en CLI)
extraction des informations IP

adresse IP et masque
calcul autour de IP : adresse de réseau, etc.


connaissances autour de/aperçu de :

un outil de diagnostic simple : ping

un outil de scan réseau : nmap

un outil qui permet d'établir des connexions "simples" (on y reviendra) : netcat

un outil pour faire des requêtes DNS : nslookup ou dig

un outil d'analyse de trafic : wireshark



manipulation simple de vos firewalls

## Conclusion :

Pour permettre à un ordinateur d'être connecté en réseau, il lui faut une liaison physique (par câble ou par WiFi).
Pour réceptionner ce lien physique, l'ordinateur a besoin d'une carte réseau. La carte réseau porte une adresse MAC.

Pour être membre d'un réseau particulier, une carte réseau peut porter une adresse IP.
Si deux ordinateurs reliés physiquement possèdent une adresse IP dans le même réseau, alors ils peuvent communiquer.

Un ordintateur qui possède plusieurs cartes réseau peut réceptionner du trafic sur l'une d'entre elles, et le balancer sur l'autre, servant ainsi de "pivot". Cet ordinateur est appelé routeur.
Il existe dans la plupart des réseaux, certains équipements ayant un rôle particulier :

un équipement appelé passerelle. C'est un routeur, et il nous permet de sortir du réseau actuel, pour en joindre un autre, comme Internet par exemple
un équipement qui agit comme serveur DNS : il nous permet de connaître les IP derrière des noms de domaine
un équipement qui agit comme serveur DHCP : il donne automatiquement des IP aux clients qui rejoigne le réseau
chez vous, c'est votre Box qui fait les trois :)